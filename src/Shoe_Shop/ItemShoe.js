import React, { Component } from "react";
import { connect } from "react-redux";
import { addToCartAction } from "./redux/action/shoeAction";

class ItemShoe extends Component {
  render() {
    let { image, name, price } = this.props.item;
    return (
      <div className="col-3 p-4">
        <div className="card ">
          <img src={image} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">Price: {price}</p>
            <button
              onClick={() => {
                this.props.handlePushToCart(this.props.item);
              }}
              className="btn btn-primary"
            >
              Add to cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handlePushToCart: (shoe) => {
      dispatch(addToCartAction(shoe));
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemShoe);
