import React, { Component } from "react";
import Cart from "./Cart";
import ListShoe from "./ListShoe";

export default class Shoe_Shop extends Component {
  render() {
    return (
      <div className="container">
        <h2>{this.props.children}</h2>
        <Cart />
        <ListShoe />
      </div>
    );
  }
}
