import { ADD_TO_CART, DECREASE_ITEM, DELETE_ITEM, INCREASE_ITEM } from "../constants/shoeConstants"

export const addToCartAction = (shoe) => {
    return{
        type: ADD_TO_CART,
        payload: shoe,
    }
}

export const deleteToCartAction = (id) => {
    return{
        type: DELETE_ITEM,
        payload: id,
    }
}

export const increaseItemAction = (id) => {
    return{
        type: INCREASE_ITEM,
        payload: id,
    }
}

export const decreaseItemAction = (id) => {
    return{
        type: DECREASE_ITEM,
        payload: id,
    }
}
