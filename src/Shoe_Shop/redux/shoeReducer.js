import { dataShoe } from "../data_shoe";
import * as types from "./constants/shoeConstants";
let initialValue = {
  listShoe: dataShoe,
  cart: [],
};

export const shoeReducer = (state = initialValue, action) => {
  switch (action.type) {
    case types.ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index === -1) {
        let newShoe = { ...action.payload, soLuong: 1 };
        cloneCart.push(newShoe);
      } else {
        cloneCart[index].soLuong++;
      }
      return {
        ...state,
        cart: cloneCart,
      };
    }
    case types.INCREASE_ITEM: {
      let { id } = action.payload;
      let { cart } = state;
      let index = cart.findIndex((item) => item.id === id);
      let updatedItem = { ...cart[index], soLuong: cart[index].soLuong + 1 };
      let updatedCart = [...cart];
      updatedCart[index] = updatedItem;
      return { ...state, cart: updatedCart };
    }
    case types.DECREASE_ITEM: {
      let { cart } = state;
      let cloneCart = [...cart];
      let index = cloneCart.findIndex((i) => i.id === action.payload.id);
      if (cloneCart[index].soLuong > 1) {
        cloneCart[index].soLuong--;
      } else {
        cloneCart.splice(index, 1);
      }
      return { ...state, cart: cloneCart };
    }
    case types.DELETE_ITEM: {
      let newCart = state.cart.filter((item) => {
        return item.id != action.payload;
      });
      return { ...state, cart: newCart };
    }
    default:
      return state;
  }
};
